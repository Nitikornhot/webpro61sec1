<!--
<?php
	echo "Hello world\n"; 

	$a = 123;
	var_dump($a);
	echo "<br>";

	echo "<h4>This is a simple heading.</h4>";
	echo "<h4 style='color: red;'>This is heading with style.</h4>";

	$color = array("Red","Green","Blue");
	var_dump($color);
	echo "<br>";

	$color_codes = array("Red" => "#ff0000",
						 "Green" => "#00ff00",
						 "Blue" => "#0000ff");
	var_dump($color_codes);

	echo "<br>";

	class greeting
	{
		
		public $str = "Hello world!";

		function show_greeting(){
			return $this -> str;
		}
	}

	$message = new greeting;
	var_dump($message);
	
	echo "<br>";
	$a = NULL;
	var_dump($a);
	echo "<br>";

	$b = "Hello world";
	$b = NULL;
	var_dump($b);



?>
-->

<!DOCTYPE html>
<html lang = "en">
<head>
		<title>Example of PHP POST method</title>
</head>
<body>
	<?php
	if(isset($_POST["name"])){
		echo "<p>Hi, " . $_POST["name"] . "</p>";
	}
	?>

	<form method="post" action="<?php echo $_SERVER["PHP_SELF"];?>">
		<label for = "inputName">Name:</label>label>
		<input type = "text" name="name" id="inputName">
		<input type="submit" value="submit">
	</form>
	</body>